/**
 * Created by Administrator on 2017/6/26.
 */
import originJsonp from 'jsonp'

/**
 *将url,data,option分开,可以更方便传入指定data(?后面跟的参数),option(包含了callback名称-->jsonpCallback:jsonp1)
 */
export default function jsonp(url, data, option) {
  url += (url.indexOf('?') < 0 ? '?' : '&') + parama(data)
  return new Promise((resolve, reject) => {
    originJsonp(url, option, (err, data) => {
      if (!err) {
        resolve(data)
      } else {
        reject(err)
      }
    })
  })
}
/**
 *
 * @param data
 * ${k}-->模板字符串,代替了传统的'玩家'+name+'未授权'
 */
function parama(data) {
  let allParama = ''
  for (let k in data) {
    let value = data[k] !== undefined ? data[k] : ''
    allParama += '&' + k + '=' + encodeURIComponent(value)
  }
  return allParama ? allParama.substring(1) : ''
}
